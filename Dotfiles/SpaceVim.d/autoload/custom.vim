function! custom#before() abort
  let g:spacevim_auto_disable_touchpad = 0
  set expandtab
  set tabstop=4
  set shiftwidth=4
  set colorcolumn=80,120
endfunction

function! custom#after() abort
  hi Normal      ctermbg=NONE guibg=NONE
  hi LineNr      ctermbg=NONE guibg=NONE
  hi SignColumn  ctermbg=NONE guibg=NONE
  hi EndOfBuffer ctermbg=NONE guibg=NONE
  set noic
  set list
  set listchars=tab:.\ ,eol:¬,trail:\ ,nbsp:¤
endfunction
