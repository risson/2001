#!/usr/bin/python

from string import ascii_lowercase, ascii_uppercase, ascii_letters, digits, punctuation
import time
import sys
import getopt
from itertools import product


def main(argv):
    dico = ''
    m = ascii_lowercase
    M = ascii_uppercase
    d = digits
    p = punctuation + ' '
    HELP = ''
    length = 5
    filename = 'gen/pswd_'
    try:
        opts, args = getopt.getopt(argv, "hmMdpc:", ["custom="])
    except getopt.GetoptError:
        print(HELP)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-m':
            dico += m
            filename += 'm_'
        elif opt == '-M':
            dico += M
            filename += 'M_'
        elif opt == '-d':
            dico += d
            filename += 'd_'
        elif opt == '-p':
            dico += p
            filename += 'p_'
        elif opt == '-h':
            print(HELP)
        elif opt in ('-c', '--custom'):
            dico += arg
            filename += 'c_/\\' + arg + '/\\_'
    if dico == '':
        dico = ascii_letters + digits
        filename += 'm_M_d_'
    
    valid_input = False
    while not valid_input:
        try:
            length = int(input('Enter the length of the passwords you want to generate : '))
        except:
            print('You have to enter an integer')
        else:
            valid_input = True
    valid_input = False
    while not valid_input:
        print('Are you sure you want to create a list of all password that can contains these characters :\n', dico, '\n and of a length of', str(length), '? (yes/no)')
        user_validation = input().lower()
        if user_validation == 'yes' or user_validation == 'y':
            valid_input = True
        elif user_validation == 'no' or user_validation == 'n':
            sys.exit(0)
        else:
            print('Wrong answer... Try again')
    filename += str(length) + '.txt'
    file = open(filename, 'a')
    start_time = time.time()
    for i in range(1, length + 1):
        for per in product(dico, repeat=i):
            #print('Current password :', per)
            pswd = ''
            for letter in per:
                pswd += letter
            file.write(pswd + '\n')
    print("--- %s seconds ---" % (time.time() - start_time))
    file.close()
     
if __name__ == '__main__':
    main(sys.argv[1:])
