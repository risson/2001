#!/usr/bin/python
import turtle as t
from random import randint, sample, choice
from sys import exit
from time import sleep

painter = t.Turtle()
painter.speed(0)
t.colormode(255)
colors = ['red', 'blue', 'green', 'pink', 'yellow', 'violet', 'black', 'orange', 'gray', 'brown', 'white']
MOD = choice([colors, 'rgb'])
# print(MOD)


def choose_mod():
    global MOD, colors
    mod_choice = input("Which one done you want ?\n"
                       "1. Random RGB\n"
                       "2. Random colors between those : Red, Blue, Green, Pink, "
                       "Yellow, Violet, Black, Orange, Gray, Brown, White\n"
                       "3. Only Red, Green and Blue\n"
                       "4. Let me choose my colors\n"
                       "5. Random choice\n"
                       "Choice: ")
    if mod_choice == '1':
        MOD = 'rgb'
    elif mod_choice == '2':
        MOD = sample(colors, randint(1, len(colors)))
    elif mod_choice == '3':
        MOD = ['red', 'blue', 'green']
    elif mod_choice == '4':
        MOD = []
        for color in colors:
            choose = input(color + "? (Y/N)")
            if choose.upper() == "Y":
                MOD.append(color)
    
    elif mod_choice == '5':
        MOD = choice([sample(colors, randint(1, len(colors))), 'rgb'])
    else:
        print("Invalid response.")
        choose_mod()


def choose_size():
    tiped = input("Type the number of circles you want: ")
    try:
        return int(tiped)
    except:
        print("Invalid response.")
        choose_size()


def draw_circle(color, size):
#    print("It's color: " + color)
    painter.pencolor(color)
    for i in range(50):
        painter.forward(size)
        painter.left(123)


def draw_circles(size):
    global MOD
    for i in range(size):
        print("Drawing circle number " + str(i+1))
        if MOD == 'rgb':
            draw_circle((randint(0, 255), randint(0, 255), randint(0, 255)), i)
        else:
            draw_circle(choice(MOD), i)


def save_img():
    print("Done.")
    save = input("Would you like to save this drawing? (Y/N) \n")
    if save.upper() == "Y":
        painter.hideturtle()
        name = input("What would you like to name it? \n")
        name_save = name + ".eps"
        ts = painter.getscreen()
        ts.getcanvas().postscript(file=name_save)
    elif save.upper() == "N":
        def run_chk():
            run_again = input("Would you like to run again? (Y/N) (N will exit)")
            if run_again.upper() == "Y":
                print("Running...")
                main()
            elif run_again.upper() == "N":
                print("Exiting...")
                exit()
            else:
                print("Invalid response.")
                run_chk()
        run_chk()
    else:
        print("Invalid response.")
        save_img()


def main():
    choose_mod()
    draw_circles(choose_size())
    save_img()
    print("Done.")
    print("Exiting...")
    sleep(5)
    exit()

if __name__ == '__main__':
    main()
