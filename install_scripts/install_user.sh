#!/bin/bash

# Errors are fatal
set -e

source common.sh

echo_status "Starting to install user"

this_script_must_be_run_as_root

echo_status ">>> Trying to load config file"
if source install.conf; then
    echo_ok ">>> Config file loaded successfully"
else
    echo_ko ">>> No config file found"
    echo_status ">>> Asking user for information"
    
    echo ">>> Enter username:"
    read username

    echo ">>> Enter password:"
    read -s password1

    echo ">>> Repeat password:"
    read -s password2

    # Check if passwords match
    if [ "$password1" != "$password2" ]; then
        echo_ko ">>> Passwords do not match. Please start over"
        exit 1
    fi

    echo ">>> Enter email:"
    read email

    echo ">>> Enter URL form which to retrieve trusted SSH keys:"
    read ssh_trusted_keys_url
fi

echo_status ">>> Creating user with supplied information"
if ! id -u $username; then
    useradd -m --groups users,wheel $username
    echo "$username:$password1" | chpasswd
fi
echo_ok ">>> Successfully created user"

if ask ">>> Do you want to give sudo rights to $username?" Y; then
    if ! which sudo; then
        echo_ko ">>>>>> sudo is not installed"
        if ask ">>>>>> Do you want to install sudo now?" Y; then
            pacman --noconfirm --needed --sync sudo
        else
            echo_ko ">>>>>> Cannot continue, please try again"
            exit 1
        fi
    fi
    sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
    echo_ok ">>> sudo rights were successfully given to $username"
fi

if ask ">>> Do you want to install Dotfiles for $username?" Y; then
    sudo -i -u $username git clone --depth 1 \
        https://gitlab.com/risson/2001.git "/home/$username/2001"
    sudo -i -u $username sudo \
        "/home/$username/2001/install_scripts/install_dotfiles.sh" \
        "/home/$username"
fi

echo_status ">>> Changing shell to /bin/zsh"
chsh -s /bin/zsh $username

if ask ">>> Do you want to create an SSH key for $username?" Y; then
    if ! which ssh; then
        echo_ko ">>>>>> openssh is not installed"
        if ask ">>>>>> Do you want to install openssh now?" Y; then
            pacman --noconfirm --needed --sync opensh
        else
            echo_ko ">>>>>> Cannot continue, please try again"
            exit 1
        fi
    fi
    sudo -i -u $username ssh-keygen -t rsa -b 4096 -C "$email"
    echo_ok ">>> SSH key successfully generated for $username"
fi

systemctl --no-ask-password enable sshd                                         
systemctl --no-ask-password start sshd

if ask ">>> Do you want to allow SSH access to this user?" Y; then
    curl "$ssh_trusted_keys_url" >> "/home/$username/.ssh/authorized_keys"
fi

chown -R $username:$username /home/$username

if ask ">>> Do you want to create a GPG key for $username?" Y; then
    if ! which gpg; then
        echo_ko ">>>>>> GnuPG is not installed"
        if ask ">>>>>> Do you want to install GnuPG now?" Y; then
            pacman --noconfirm --needed --sync gnupg
        else
            echo_ko ">>>>>> Cannot continue, please try again"
            exit 1
        fi
    fi
    sudo -i -u $username gpg --list-keys
    sudo -i -u $username touch "/home/$username/.gnupg/gpg.conf" \
                               "/home/$username/.gnupg/gpg-agent.conf"
    echo "keyserver-options auto-key-retrieve" >> "/home/$username/.gnupg/gpg.conf"
    echo "keyserver hkp://pgp.mit.edu" >> "/home/$username/.gnupg/gpg.conf"
    echo "pinentry-program /usr/bin/pinentry-curses" >> "/home/$username/.gnupg/gpg-agent.conf"
    chown -R $username:$username /home/$username
    sudo -i -u $username gpg-connect-agent reloadagent /bye
    sudo -i -u $username gpg --full-generate-key --homedir "/home/$username"
    echo_ok ">>> GPG key successfully generated for $username"
fi

chown -R $username:$username /home/$username
echo_ok "Successfully installed user $username"

# END
