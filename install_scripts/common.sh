# Source this file

# Errors are fatal
set -e


##### Checking functions #####

function this_script_must_be_run_as_root {
    if [ $(id -u) -ne 0 ]; then
        echo >&2 "[!] This script must be run as root"
        return 1
    fi
}

function ask {
    # https://gist.github.com/davejamesmiller/1965569
    local prompt default reply

    if [ "${2:-}" = "Y" ]; then
        prompt="Y/n"
        default=Y
    elif [ "${2:-}" = "N" ]; then
        prompt="y/N"
        default=N
    else
        prompt="y/n"
        default=
    fi

    while true; do

        # Ask the question (not using "read -p" as it uses stderr not stdout)
        echo -n "$1 [$prompt] "

        # Read the answer (use /dev/tty in case stdin is redirected from somewhere else)
        read reply </dev/tty

        # Default?
        if [ -z "$reply" ]; then
            reply=$default
        fi

        # Check if the reply is valid
        case "$reply" in
            Y*|y*) return 0 ;;
            N*|n*) return 1 ;;
        esac

    done
}

##########


##### Logging functions #####

function echo_status {
 # blue foreground, then default foreground
  echo -e "\e[34m[+] $*\e[39m"
}

function echo_ko {
  # red foreground, then default foreground
  echo -e "\e[31m$*\e[39m"
}

function echo_ok {
  # green foreground, then default foreground
  echo -e "\e[32m$*\e[39m"
}

##########
