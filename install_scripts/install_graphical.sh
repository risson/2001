#!/bin/bash

# Errors are fatal
set -e

source common.sh

echo_status "Starting graphical installation"

this_script_must_be_run_as_root

if ! [ "$(ping -c 1 archlinux.org)" ]; then
    echo_ko ">>> Please check your internet connection"
    exit 1
fi

echo_status ">>> Installing packages"
pacman_packages=()
aur_packages=()
# X essentials
pacman_packages+=( xorg-server xorg-apps xorg-xinit xorg-fonts-misc xsel \
                                           xbindkeys xautolock xorg-xbacklight)
# i3 essentials
pacman_packages+=( i3-wm i3status )
# i3 utilities
pacman_packages+=( dmenu py3status polkit-gnome )
aur_packages+=( betterlockscreen )
# bar applets
pacman_packages+=( network-manager-applet  xfce4-power-manager sbxkb )
# Lots of fonts
pacman_packages+=( cairo fontconfig freetype2 ttf-hack ttf-dejavu \
    ttf-liberation ttf-inconsolata ttf-anonymous-pro ttf-ubuntu-font-family \
    ttf-croscore ttf-droid ttf-roboto adobe-source-code-pro-fonts \
    adobe-source-sans-pro-fonts adobe-source-serif-pro-fonts dina-font \
    terminus-font tamsyn-font artwiz-fonts )
aur_packages+= ( ttf-twitter-color-emoji-svginot )
# Audio
pacman_packages+=( pulseaudio alsa-utils )
# Software
pacman_packages+=( vlc gimp feh libreoffice-fresh okular libreoffice-fresh-fr \
    audacity evince filezilla spectacle terminator musescore pcmanfm )
aur_packages+=( firefox-nightly rambox-bin spotify discord jetbrains-toolbox \
    rescuetime visual-studio-code-bin )
# Actual install
pacman --noconfirm --needed --sync ${pacman_packages[@]}
pikaur --noconfirm --needed --sync ${aur_packages[@]}
echo_ok ">>> Successfully installed packages"

echo_status ">>> Configuring shell to ZSH"
chsh -s /bin/zsh
echo_ok ">>> Successfully configured shell"

echo_status ">>> Configuring locale, timezone, NTP and hostname"
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen
localectl --no-ask-password set-locale LANG="en_US.UTF-8"
timedatectl --no-ask-password set-timezone Europe/Paris
timedatectl --no-ask-password set-ntp 1
hostnamectl --no-ask-password set-hostname $hostname
echo_ok ">>> Successfully configured locale, timezone, NTP and hostname"

if ask "Do you want to install a user?" Y; then
    ./install_user.sh
fi

if ask "Do you want to install graphical stuff?"; then
    ./install_graphical.sh
fi

systemctl --no-ask-password enable sshd
systemctl --no-ask-password start sshd

echo_status ">>> Configuring SSH for root"
if ask "Do you want to enable SSH for root?" N; then
    echo "PermitRootLogin prohibit-password" >> /etc/ssh/sshd_config
    curl "$ssh_trusted_keys_url" >> $HOME/.ssh/authorized_keys
else
    systemctl --no-ask-password disable sshd
    systemctl --no-ask-password stop sshd
fi
echo_ok ">>> Fininshed configuring SSH"

echo_status ">>> Removing orphans packages"
if [[ ! -n $(pacman -Qdt) ]]; then
    echo_status "No orphans to removed"
else
    pacman -Rns $(pacman -Qdtq)
fi
echo_ok ">>> Successfully removed oprhans packages"

echo_ok "Successfully installed new system!"

#END
