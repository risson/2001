#!/bin/bash

# Errors are fatal
set -e

source common.sh

echo_status "Starting installation of new system!"

this_script_must_be_run_as_root

if ! [ "$(ping -c 1 archlinux.org)" ]; then
    echo_ko ">>> Please check your internet connection"
    exit 1
fi

echo_status ">>> Trying to load config file"
if source install.conf; then
    echo_ok ">>> Config file loaded successfully"
else
    echo_ko ">>> No config file found"
    echo_status ">>> Asking user for information"

    echo ">>> Enter hostname:"
    read hostname

    echo ">>> Enter URL form which to retrieve trusted SSH keys:"
    read ssh_trusted_keys_url
fi

echo_status ">>> Configuring Pacman"
pacman --noconfirm --sync reflector rsync
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
reflector --protocol http --protocol https --protocol ftp --protocol rsync \
                          --sort rate --save /etc/pacman.d/mirrorlist --verbose
sed -i 's/^#Color/Color\
ILoveCandy\
/' /etc/pacman.conf
echo_ok ">>> Successfully configured Pacman"

echo_status ">>> Installing packages"
pacman_packages=()
# base{,-devel}
pacman_packages+=( base base-devel linux-headers )
# Essentials
pacman_packages+=( dbus )
# Admin tools
pacman_packages+=( sudo pacman-contrib git zsh tmux openssh ntfs-3g tree htop \
    mosh )
# Network tools
pacman_packages+=( wpa_actiond wpa_supplicant syncthing net-tools )
# Dev tools
pacman_packages+=( vim make nodejs npm python python-pip astyle doxygen gcc \
    valgrind git-lfs pandoc php nginx pyalpm sqlite mariadb )
# Daily life tools
pacman_packages+=( zip unzip tar elinks ffmpeg youtube-dl )
# Actual install
pacman --noconfirm --needed --sync ${pacman_packages[@]}
git clone --depth=1 https://github.com/actionless/pikaur.git
cd pikaur
python ./pikaur.py --noconfirm --needed --sync pikaur
cd ..
rm -rf pikaur
echo_ok ">>> Successfully installed packages"

echo_status ">>> Configuring shell to ZSH"
chsh -s /bin/zsh
echo_ok ">>> Successfully configured shell"

echo_status ">>> Configuring locale, timezone, NTP and hostname"
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen
localectl --no-ask-password set-locale LANG="en_US.UTF-8"
timedatectl --no-ask-password set-timezone Europe/Paris
timedatectl --no-ask-password set-ntp 1
hostnamectl --no-ask-password set-hostname $hostname
echo_ok ">>> Successfully configured locale, timezone, NTP and hostname"

if ask "Do you want to install a user?" Y; then
    ./install_user.sh
fi

if ask "Do you want to install graphical stuff?"; then
    ./install_graphical.sh
fi

systemctl --no-ask-password enable sshd
systemctl --no-ask-password start sshd

echo_status ">>> Configuring SSH for root"
if ask "Do you want to enable SSH for root?" N; then
    echo "PermitRootLogin prohibit-password" >> /etc/ssh/sshd_config
    curl "$ssh_trusted_keys_url" >> $HOME/.ssh/authorized_keys
fi
echo_ok ">>> Fininshed configuring SSH"

echo_status ">>> Removing orphans packages"
if [[ ! -n $(pacman -Qdt) ]]; then
    echo_status "No orphans to removed"
else
    pacman -Rns $(pacman -Qdtq)
fi
echo_ok ">>> Successfully removed oprhans packages"

echo_ok "Successfully installed new system!"

#END
