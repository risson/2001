#/bin/bash

# Errors are fatal
set -e

path_to_install="$HOME"

if [ $1 ]; then
    path_to_install="$1"
fi

source "$path_to_install/2001/install_scripts/common.sh"

echo_status "Starting to install Dotfiles"

this_script_must_be_run_as_root

echo_status ">>> Installing dependencies"
# Installing dependencies
pacman --noconfirm --needed --sync python python-pip \
              zsh zsh-completions zsh-syntax-highlighting zsh-autosuggestions
#pikaur --noconfirm --needed --sync epifortune
pip install -U dotfiles

echo_status ">>> Getting git submodules"
git -C "$path_to_install/2001" submodule init
git -C "$path_to_install/2001" submodule sync
git -C "$path_to_install/2001" submodule update

echo_status ">>> Synchronizing Dotfiles"
dotfiles -R "$path_to_install/2001/Dotfiles" -H "$path_to_install" \
    -C "$path_to_install/2001/Dotfiles/dotfilesrc" -c
dotfiles -R "$path_to_install/2001/Dotfiles" -H "$path_to_install" \
    -C "$path_to_install/2001/Dotfiles/dotfilesrc" -sf

echo_ok "Dotfiles successfully installed"

# END
